let campoMensagem = document.querySelector(".areaText");
let botaoSend = document.querySelector(".sendButton");
let areaChat = document.querySelector(".msgScreen");


botaoSend.addEventListener("click", () => {
    enviarMensagem();
});


function enviarMensagem() {
    let novoCampo = document.createElement("div");
    novoCampo.className = "boxMensagem";
    let texto = document.createElement("p");
    texto.className = "textoMensagem";
    texto.innerText = campoMensagem.value;
    var botaoExcluir = document.createElement("button");
    botaoExcluir.className = "botaoExcluir";
    var botaoEditar = document.createElement("button");
    botaoEditar.className = "botaoEditar";
    novoCampo.appendChild(texto);
    novoCampo.appendChild(botaoExcluir);
    novoCampo.appendChild(botaoEditar);
    areaChat.appendChild(novoCampo);
    campoMensagem.value = "";

    botaoExcluir.addEventListener("click", () => {
        areaChat.removeChild(novoCampo);
    });

    botaoEditar.addEventListener("click", () => {
        novoCampo.innerHTML = "";
        var campoEditar = document.createElement("textarea");
        var botaoEditarMsg = document.createElement("button");
        campoEditar.className = "editAreaText";
        botaoEditarMsg.className = "botaoEditarMsg";
        botaoEditarMsg.innerText = "Editar";
        novoCampo.appendChild(campoEditar);
        novoCampo.appendChild(botaoEditarMsg);
    });

    botaoEditar.addEventListener("click", () => {
        novoCampo.innerHTML = "";
        var campoEditar = document.createElement("textarea");
        var botaoEditarMsg = document.createElement("button");
        campoEditar.className = "editAreaText";
        campoEditar.value = texto.innerText;
        botaoEditarMsg.className = "botaoEditarMsg";
        botaoEditarMsg.innerText = "Editar";
        novoCampo.appendChild(campoEditar);
        novoCampo.appendChild(botaoEditarMsg);

        botaoEditarMsg.addEventListener("click", () => {
            texto.innerText = campoEditar.value;
            novoCampo.removeChild(campoEditar);
            novoCampo.removeChild(botaoEditarMsg);
            novoCampo.appendChild(texto);
            novoCampo.appendChild(botaoExcluir);
            novoCampo.appendChild(botaoEditar);
        });

    });
}


